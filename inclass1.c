#include<reg51.h>

sbit tf1_red = P2^0;
sbit tf1_yello = P2^1;
sbit tf1_green = P2^2;

sbit tf2_red = P2^4;
sbit tf2_yello = P2^5;
sbit tf2_green = P2^6;

sbit tf3_red = P1^0;
sbit tf3_yello = P1^1;
sbit tf3_green = P1^2;

sbit tf4_red = P1^4;
sbit tf4_yello = P1^5;
sbit tf4_green = P1^6;

void delay(int tick){
	int i,j;
	for(i=0;i<tick;i++)
		for(j=0;j<20;j++);
}

void main(){
	while(1){
		P2 = 0;
		P1 = 0;
		tf1_green = 1;
		tf2_red = 1;
		tf3_red = 1;
		tf4_red = 1;
		delay(10000);
		
		P2 = 0;
		P1 = 0;
		tf1_yello = 1;
		tf2_red = 1;
		tf3_red = 1;
		tf4_red = 1;
		delay(10000);
		
		P2 = 0;
		P1 = 0;
		tf1_red = 1;
		tf2_green = 1;
		tf3_red = 1;
		tf4_red = 1;
		delay(10000); 	

		P2 = 0;
		P1 = 0;
		tf1_red = 1;
		tf2_yello = 1;
		tf3_red = 1;
		tf4_red = 1;
		delay(10000);
		
		P2 = 0;
		P1 = 0;
		tf1_red = 1;
		tf2_red = 1;
		tf3_green = 1;
		tf4_red = 1;
		delay(10000);
		
		P2 = 0;
		P1 = 0;
		tf1_red = 1;
		tf2_red = 1;
		tf3_yello = 1;
		tf4_red = 1;
		delay(10000); 

		P2 = 0;
		P1 = 0;
		tf1_red = 1;
		tf2_red = 1;
		tf3_red = 1;
		tf4_green = 1;
		delay(10000);

		P2 = 0;
		P1 = 0;
		tf1_red = 1;
		tf2_red = 1;
		tf3_red = 1;
		tf4_yello = 1;
		delay(10000);
	}
}